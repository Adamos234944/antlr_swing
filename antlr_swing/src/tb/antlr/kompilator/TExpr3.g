tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                  -> addition(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                  -> subtraction(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                  -> multiplication(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                  -> division(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)                  -> write_variable(i1={$i1.text},p2={$e2.st})
        | INT                                       -> int(i={$INT.text})
        | ID                                        -> read_variable(i={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?)            -> if_statement(condition = {$e1.st}, true_block={$e2.st}, false_block={$e3.st},nr={numer.toString()})
        | ^(GREATER e1=expr e2=expr){numer++;}      -> is_greater(p1={$e1.st},p2={$e2.st},nr={numer.toString()})
        | ^(SMALLER e1=expr e2=expr){numer++;}      -> is_smaller(p1={$e1.st},p2={$e2.st},nr={numer.toString()})
        | ^(EQUAL e1=expr e2=expr){numer++;}        -> is_equal(p1={$e1.st},p2={$e2.st},nr={numer.toString()})
        | ^(NOTEQUAL e1=expr e2=expr){numer++;}     -> is_not_equal(p1={$e1.st},p2={$e2.st},nr={numer.toString()})
    ;
    