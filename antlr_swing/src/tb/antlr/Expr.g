grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    | if_statement
//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID (PODST expr)? NL-> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | NL->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
 
if_statement
    : IF^ LP! (boolean_comparison) RP! LB! stat RB! (ELSE! LB! stat RB!)?;
boolean_comparison
    : expr
      ( GREATER^ expr
      | SMALLER^ expr
      | EQUAL^ expr
      | NOTEQUAL^ expr
      )
    ;
multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

GREATER : '>' ;
SMALLER : '<' ;
EQUAL : '==' ;
NOTEQUAL : '!=' ;

VAR :'var';

IF
  : 'if'
  ;
  
ELSE
  : 'else'
  ;
  
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : ';' ;

WS : (' ' | '\t' | '\n')+ {$channel = HIDDEN;} ;


  
LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;
	
DIV
	:	'/'
	;
	

LB
  : '{'
  ;
 
RB
  : '}'
  ;
